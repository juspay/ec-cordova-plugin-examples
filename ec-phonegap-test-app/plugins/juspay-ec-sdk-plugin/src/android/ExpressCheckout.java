package com.apache.cordova.expresscheckout;

import android.content.Context;
import android.content.Intent;

import org.apache.cordova.CallbackContext;
import org.apache.cordova.CordovaPlugin;
import org.json.JSONArray;
import org.json.JSONObject;
import org.json.JSONException;

import in.juspay.juspaysafe.BrowserCallback;
import in.juspay.godel.analytics.GodelTracker;

import in.juspay.ec.sdk.MobileWebCheckout;
import in.juspay.ec.sdk.util.Environment;

public class ExpressCheckout extends CordovaPlugin {

    private static final String PAYMENT = "JuspayStartPayment";
    private static final String TRACK_STATUS = "JuspayTrackStatus";

    @Override
    public boolean execute(String action, JSONArray args, final CallbackContext callbackContext) throws JSONException {
        System.out.println(args.toString());
        try {
            if(action.equals(PAYMENT)) {
                doPayment(args.optJSONObject(0), callbackContext);
                return true;
            } else if(action.equals(TRACK_STATUS)) {
                trackStatus(args.optJSONObject(0));
                return true;                
            }
        } catch(JSONException e) {
            e.printStackTrace();
            throw e;
        }

        return false;
    }

    private void doPayment(JSONObject params, final CallbackContext callbackContext) throws JSONException {
        JSONArray endUrlRegexJSON = params.optJSONArray("endUrlRegexes");
        String[] endUrlRegexes = null;

        if(endUrlRegexJSON != null) {
            endUrlRegexes = new String[endUrlRegexJSON.length()];

            for(int i=0;i<endUrlRegexJSON.length();i+=1) {
                endUrlRegexes[i] = endUrlRegexJSON.getString(i);
            }
        }        
        
        System.out.println(params.toString());

        MobileWebCheckout checkout = new MobileWebCheckout(
            Environment.valueOf(params.getString("environment")),
            params.getString("merchantId"),
            params.getString("orderId"),
            endUrlRegexes
        );

        /**
        * Here we ensure that we call the same function by specifiying the same callback 
        * on JS side
        */
        checkout.startPayment(this.cordova.getActivity(), new BrowserCallback() {
            @Override
            public void endUrlReached(String var1) {
                callbackContext.success(var1);
            }

            @Override
            public void ontransactionAborted() {                        
                callbackContext.error("txn-aborted");
            }
        });
    }

    private void trackStatus(JSONObject params) throws JSONException {
        GodelTracker
            .getInstance()
            .trackPaymentStatus(
                params.getString("txnId"), 
                GodelTracker.PaymentStatus.valueOf(params.getString("paymentStatus"))
            );
    }
}