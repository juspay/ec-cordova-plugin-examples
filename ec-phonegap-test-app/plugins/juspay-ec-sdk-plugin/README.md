# PhoneGap app integration

JusPay has a native android client which can be used by PhoneGap applications. To get started, you first need to download the code for the plugin.

## Installation
This requires phonegap/cordova CLI 5.0+ (current stable v1.5.3).

Install via repo url directly (latest version)
```sh
phonegap plugin add https://bitbucket.org/juspay/ec-cordova-plugin
```
or
```sh
cordova plugin add https://bitbucket.org/juspay/ec-cordova-plugin
```
## Android Details
### Pre Requisites

Our android client requires a minimum Android SDK version of **8**. To make this change, you have to edit `AndroidManifest.xml` or `config.xml` of your app to make these changes.

 * For PhoneGap, edit the `config.xml` file. By default PhoneGap sets the version to 14. You may set this to **8**.
 * For Cordova, edit the `AndroidManifest.xml`

The **JusPay Safe Browser** requires an *AppCpmpat* android activity theme, so you might have to set the `android:theme` attribute in he `AndroidManifest.xml`. Please check the manifest file on either of the sample apps for examples.

In the `build.gradle` of your phonegap / cordova app, add the following dependencies:

```groovy
compile (name: 'juspay-ec-sdk-release', ext:'aar') // Express Checkout client library
compile ('in.juspay:godel:0.6.7.1.2')                // JusPay Safe browser
```
#### Express Checkout android client
This is the android client library for Express Checkout service which helps with the collection of payment information from your customers.
#### JusPay Safe browser
This is the Payment Browser which helps the customer complete the transaction easily. Features like OTP auto reading are built into this library.

The above client libraries are not available on any of the public maven repositories. Add the following line to the repositories section of your build.gradle.

Express Checkout android client library is copied into `src/libs` during installation. Now, you have to include it in the `build.gradle` file so that the project compiles with the library.

```groovy
flatDir {
  dirs 'src/libs'
}
maven {
  url 'https://s3-ap-southeast-1.amazonaws.com/godel-release/godel/'
}
```
    
### Starting payment flow

You are ready to start the payment as soon as your server has called JusPay API to create order. The following parameters are available to you now:

```javascript
var orderId = "<your_order_id>"
var merchantId = "<your_merchant_id>"
```

### Define Return Url Callback
When the user has completed the payment (could be success or failure), the user will be redirected to the `return_url` configured by you at the time of order creation. JusPay will invoke your function when the user has reached this `return_url`. 
```javascript
var onEndUrlReached = function () {
    // your code to check the server status
    var paymentStatus = getPaymentStatusFromServer()
    if(paymentStatus == "CHARGED") {
        gotoThankYouPage()
    }
    else {
        gotoFailurePage()
    }
};
```
**Note**: JusPay will not provide the payment status in the callback function, because it is a security issue. You will have to check with your server and retrieve the status from JusPay using `/order/status` API.
### Define back button callback
If the user presses back button, then the transaction is aborted midway by the user. Our plugin will let you know when this happens through a callback. You may define the function as:
```javascript
var abortedCallback = function () {
    gotoFailurePage()
};
```
The plugin will handle all the payment pages and when the user has completed the payment, the user is finally redirected to your website. To stop the plugin at the correct end URL, you must declare the final return URL that you have configured with JusPay.
```javascript
var endUrls = ["https://shop.com/juspay-response/.*", "https://beta.shop.com/juspay-response/.*"]
```
Once all these variables are declared correctly, you are ready to put it together and setup the payment flow:

```javascript
ExpressCheckout.startCheckoutActivity({
    "endUrls": endUrls,
    "onEndUrlReached": onEndUrlReached,
    "onTransactionAborted": onTransactionAborted,
    "environment": "PRODUCTION", // can be either of "SANDBOX" or "PRODUCTION"
    "parameters": {
        "orderId": orderId,
        "merchantId": merchantId,
    },
})
```

## Example project

We have setup a simple example for you to see the code working end to end. You can check the example [here](https://bitbucket.org/).

## Help & Support

If you notice any errors or issues with the integration, please reach out to us at support@juspay.in. You may also search our [Knowledge base](http://faq.juspay.in) to see if the issue has already been addressed by our team.
