//
//  JPExpressCheckout.m
//  
//
//  Created by Sachin Sharma on 30/04/16.
//
//

#import "JPExpressCheckout.h"

@implementation JPExpressCheckout
-(void)mobileWebCheckout:(CDVInvokedUrlCommand*)command{
    
    NSString* callbackId = command.callbackId;
    NSString* environment = [[command arguments] objectAtIndex:0];
    NSString* merchantId = [[command arguments] objectAtIndex:1];
    NSString* orderId = [[command arguments] objectAtIndex:2];
    NSArray* endUrlRegexes = [[command arguments] objectAtIndex:3];
    
    ExpressCheckout *checkout = [[ExpressCheckout alloc] init];
    [checkout mobileWebCheckoutForEnvironment:[checkout environmentEnumFromString:environment] merchantID:merchantId orderID:orderId endURLRegex:endUrlRegexes];
    [checkout startPaymentInView:self.webView callback:^(BOOL status, NSError *error, id info) {
        
        NSMutableDictionary *dict = [[NSMutableDictionary alloc]init];
        if (error) {
            [dict setObject:error forKey:@"error"];
        }
        [dict setObject:[NSNumber numberWithBool:status] forKey:@"status"];
        [dict setObject:info forKey:@"info"];
        
        
        CDVPluginResult* result = [CDVPluginResult
                                   resultWithStatus:CDVCommandStatus_OK
                                   messageAsDictionary:dict];
        [self.commandDelegate sendPluginResult:result callbackId:callbackId];
        
    }];
}
@end
