### 0.0.3

* Removed examples tarball
* Added [link to examples repository.](https://bitbucket.org/juspay/ec-cordova-plugin-examples)

### 1.0.1

* Updated SDK to take in the Payment instruments

### 1.0.3

* Added choice for payment mode

### 1.0.4

* Added iOS platform

### 1.0.5

* Fixed issue for framework for iOS platform
