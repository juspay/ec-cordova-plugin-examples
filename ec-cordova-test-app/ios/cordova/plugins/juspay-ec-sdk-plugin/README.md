# Juspay Express Checkout Cordova / Phonegap Plugin

## Sample Apps

Cordova: []()

Phonegap []()

## Setting Up
#### Adding via git:
```bash
    cordova plugin add https://sri-jay@bitbucket.org/sri-jay/ec-cordova-plugin.git
```
#### Or
```bash
    git clone https://sri-jay@bitbucket.org/sri-jay/ec-cordova-plugin.git
    cordova plugin add ec-cordova-plugin/
```

#### Android version

Set the **minimum sdk version** to 16. You might have to edit the **AndroidManifest.xml** or the **config.xml** of your app to make these changes.
1) For **phonegap**, edit the **config.xml** file. By default phonegap sets the version to 14
2) For **cordova**, edit the **AndroidManifest.xml**

#### **Important**: Proguard and Godel

Once you have built an apk of your application, test it on the [godel-verifier](http://godel-verifier.juspay.in/) to ensure a smooth integration.

#### Changes to **build.gradle**

In the `build.gradle` of your phonegap / cordova app, add the following dependencies:
```gradle
    compile(name: 'juspay-ec-sdk-release', ext:'aar')
    compile ('in.juspay:godel:0.6.7.2')
```

`juspay-ec-sdk` and `in.juspay:godel` are not available on any of the public maven repositories. Add the following line to the `repositories` section of your `build.gradle`.

The `flatDir` is added to get the packaged `juspay-ec-sdk` aar, bundled with the plugin avaiable as a local repository.

```gradle
    flatDir {
        dirs 'src/libs'
    }
    maven {
        url 'https://s3-ap-southeast-1.amazonaws.com/godel-release/godel/'
    }
```

## API

The plugin exposes a set of ***global javascript objects***.

1) `window.ExpressCheckout`

    The express checkout object exports the **startPaymentActivity**(**callback**: *function*, **options**: *Object*, **regexes**: *Array*)`
    function that can be used to start the main payment activity.
    
    The following arguments must be passed:
    1) **callback**: Callback to be executed once the payment activity ends
    2) **options**: Set the payment options. All properties specified in the `MobileCheckoutOptions` global object must be made available.
    3) **regexes**: Pass in an array of regular expressions that will be matched with the url of the browser to indicate that the payment has reached the end page.
    
2) `window.MobileCheckoutOptions`
    
    Exports a set of properties that are to be specified in the **options** passed to the `window.ExpressCheckout.startPaymentActivity` function.
    
    A shorthand method is also exposed: `optionsFor(merchantId: string, orderId: string, instruments: string, environment: string)`
    
3) `window.GodelTracker`

    The godel tracker object exports the following methods:
    1) **trackSuccess**(**txnId**: *string*)
    2) **trackFailure**(**txnId**: *string*)
    3) **trackCancelled**(**txnId**: *string*)
    
    Pass in the transactionId of the payment.
    
**Additional documents can be found at [https://apidocs.juspay.in](https://apidocs.juspay.in)**
    
    
    
    
    
    
    