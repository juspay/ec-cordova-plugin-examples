var constants = {
    MERCHANT_ID: "merchantId",
    ORDER_ID: "orderId",
    ENVIROMENT: "env",
};

/*
    This makes the options for a non enumerable property, which is very bombastic.
*/
Object.defineProperty(constants, 'optionsFor', {
    enumerable: false,
    value: function (merchantId, orderId, environment) {
        return {
            'merchantId': merchantId,
            'orderId': orderId,
            'env': environment
        };
    }
});

if('freeze' in Object && typeof Object.freeze === 'function') {
    Object.freeze(constants);
}

module.exports = constants;