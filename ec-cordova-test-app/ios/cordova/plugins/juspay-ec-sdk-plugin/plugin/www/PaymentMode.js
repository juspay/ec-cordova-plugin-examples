var paymentMethods = {
	NET_BANKING: {
		NB_AXIS: 	'NB_AXIS',
		NB_BOI:	 	'NB_BOI',
		NB_BOM:	 	'NB_BOM',
		NB_CBI:	 	'NB_CBI',
		NB_CORP: 	'NB_CORP',
		NB_DCB:	 	'NB_DCB',
		NB_FED:	 	'NB_FED',
		NB_HDFC: 	'NB_HDFC',
		NB_ICICI: 	'NB_ICICI',
		NB_IDBI: 	'NB_IDBI',
		NB_INDB: 	'NB_INDB',
		NB_INDUS: 	'NB_INDUS',
		NB_IOB:	 	'NB_IOB',
		NB_JNK:	 	'NB_JNK',
		NB_KARN: 	'NB_KARN',
		NB_KVB:	 	'NB_KVB',
		NB_SBBJ: 	'NB_SBBJ',
		NB_SBH:	 	'NB_SBH',
		NB_SBI:	 	'NB_SBI',
		NB_SBM:	 	'NB_SBM',
		NB_SBT:	 	'NB_SBT',
		NB_SOIB: 	'NB_SOIB',
		NB_UBI:	 	'NB_UBI',
		NB_UNIB: 	'NB_UNIB',
		NB_VJYB: 	'NB_VJYB',
		NB_YESB: 	'NB_YESB',
		NB_CUB:	 	'NB_CUB',
		NB_CANR: 	'NB_CANR',
		NB_SBP:	 	'NB_SBP',
		NB_CITI: 	'NB_CITI',
		NB_DEUT: 	'NB_DEUT',
		NB_KOTAK: 	'NB_KOTAK',
		NB_DLS:	 	'NB_DLS',
		NB_ING:	 	'NB_ING'
	},
	CARD: {
		VISA: 'VISA',
		MASTERCARD: 'MASTERCARD',
		MAESTRO: 'MAESTRO',
		AMEX: 'AMEX',
		RUPAY: 'RUPAY'
	},
	WALLET: {
		PAYU: 'PAYUMONEY'
	}
};

if(freeze in Object) {
	paymentMethods = Object.freeze(paymentMethods);
}

module.exports = paymentMethods;
