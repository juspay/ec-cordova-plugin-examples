//
//  JPExpressCheckout.m
//  
//
//  Created by Sachin Sharma on 30/04/16.
//
//

#import "JPExpressCheckout.h"

@implementation JPExpressCheckout
-(void)JuspayStartPayment:(CDVInvokedUrlCommand*)command{
    
    NSString* callbackId = command.callbackId;
    NSDictionary *arguments = [[command arguments] objectAtIndex:0];
    NSString* environment = [arguments objectForKey:@"environment"];
    NSString* merchantId = [arguments objectForKey:@"merchantId"];
    NSString* orderId = [arguments objectForKey:@"orderId"];
    NSArray* instruments = [arguments objectForKey:@"instruments"];
    NSArray* endUrlRegexes = [arguments objectForKey:@"endUrlRegexes"];
    
    ExpressCheckout *checkout = [[ExpressCheckout alloc] init];
    
    if ([checkout respondsToSelector:@selector(environment:merchantId:orderId:instrumentOptions:endUrlRegexes:)]) {
        [checkout environment:[checkout environmentEnumFromString:environment] merchantId:merchantId orderId:orderId instrumentOptions:instruments endUrlRegexes:endUrlRegexes];
    }else{
        [checkout environment:[checkout environmentEnumFromString:environment] merchantId:merchantId orderId:orderId endUrlRegexes:endUrlRegexes];
    }
    
    [checkout startPaymentInView:self.webView callback:^(BOOL status, NSError *error, id info) {
        
        NSMutableDictionary *dict = [[NSMutableDictionary alloc]init];
        if (error) {
            [dict setObject:error forKey:@"error"];
        }
        [dict setObject:[NSNumber numberWithBool:status] forKey:@"status"];
        [dict setObject:info forKey:@"info"];
        
        
        CDVPluginResult* result = [CDVPluginResult
                                   resultWithStatus:CDVCommandStatus_OK
                                   messageAsDictionary:dict];
        [self.commandDelegate sendPluginResult:result callbackId:callbackId];
        
    }];
}
@end
