/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */
var app = {
    // Application Constructor
    initialize: function() {
        this.bindEvents();
    },
    // Bind Event Listeners
    //
    // Bind any events that are required on startup. Common events are:
    // 'load', 'deviceready', 'offline', and 'online'.
    bindEvents: function() {
        document.addEventListener('deviceready', this.onDeviceReady, false);
    },
    // deviceready Event Handler
    //
    // The scope of 'this' is the event. In order to call the 'receivedEvent'
    // function, we must explicitly call 'app.receivedEvent(...);'
    onDeviceReady: function() {
        app.receivedEvent('deviceready');
        console.log(typeof ExpressCheckout);
    },
    // Update DOM on a Received Event
    receivedEvent: function(id) {
        var body = document.getElementsByTagName('body')[0];
        body.classList.remove('inactive');
        body.classList.add('active');

        document.querySelector('#advert').onclick = function () {
            var orderId = Date.now();
            var orderCreationRequest = new XMLHttpRequest();
            orderCreationRequest.open('POST', 'https://sandbox.juspay.in/order/create', true, '9F64B727D4C048C08013EA0935BCBDB5', '');
            orderCreationRequest.withCredentials = true;

            orderCreationRequest.setRequestHeader('Authorization', 'OUY2NEI3MjdENEMwNDhDMDgwMTNFQTA5MzVCQ0JEQjU6');
            orderCreationRequest.setRequestHeader("Content-type","application/x-www-form-urlencoded");
            
            var errorCallback = function () {
                body.classList.remove('active');
                body.classList.add('error');

                window.setTimeout(function() {
                    body.classList.remove('error');
                    body.classList.add('active');
                }, 2000);
            };

            var successCallback = function () {
                body.classList.remove('active');
                body.classList.add('success');

                window.setTimeout(function() {
                    body.classList.remove('success');
                    body.classList.add('active');
                }, 2000);     
            };


            orderCreationRequest.onreadystatechange = function () {
                if(orderCreationRequest.readyState == 4) {
                    if(orderCreationRequest.status == 200) {
                        ExpressCheckout.startCheckoutActivity({
                            endUrls: ['https:\\/\\/test.com.\\/'],
                            onEndUrlReached: function () {
                                successCallback();
                            },
                            onTransactionAborted: function () {
                                errorCallback();
                            },
                            environment: 'PRODUCTION',
                            instruments: ['CARD'],
                            parameters: {
                                orderId: '1464119848',
                                merchantId: 'sachin_sharma'
                            }
                        });  
                    } else {
                        errorCallback();
                    }
                }
            };
            orderCreationRequest.send('order_id='+orderId+'&amount=1.00');
        }            
    }
};

app.initialize();