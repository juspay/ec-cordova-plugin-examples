cordova.define('cordova/plugin_list', function(require, exports, module) {
module.exports = [
    {
        "file": "plugins/juspay-ec-sdk-plugin/plugin/www/ExpressCheckout.js",
        "id": "juspay-ec-sdk-plugin.ExpressCheckout",
        "pluginId": "juspay-ec-sdk-plugin",
        "clobbers": [
            "ExpressCheckout"
        ]
    }
];
module.exports.metadata = 
// TOP OF METADATA
{
    "cordova-plugin-whitelist": "1.2.1",
    "juspay-ec-sdk-plugin": "0.0.1"
}
// BOTTOM OF METADATA
});