### 0.0.3

* Removed examples tarball
* Added [link to examples repository.](https://bitbucket.org/juspay/ec-cordova-plugin-examples)

### 1.0.1

* Updated SDK to take in the Payment instruments