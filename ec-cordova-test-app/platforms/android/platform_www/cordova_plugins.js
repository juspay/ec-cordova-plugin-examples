cordova.define('cordova/plugin_list', function(require, exports, module) {
module.exports = [
    {
        "file": "plugins/cordova-plugin-whitelist/whitelist.js",
        "id": "cordova-plugin-whitelist.whitelist",
        "runs": true
    },
    {
        "file": "plugins/juspay-ec-sdk-plugin/plugin/www/ExpressCheckout.js",
        "id": "juspay-ec-sdk-plugin.ExpressCheckout",
        "clobbers": [
            "ExpressCheckout"
        ]
    }
];
module.exports.metadata = 
// TOP OF METADATA
{}
// BOTTOM OF METADATA
});